// Package importer controls your import block of the generated code and objects from external packages.
// It keeps full import path of the used objects and returns valid import block.
// Importer counts duplicate import aliases and remembers each path.
package importer

import (
	"bytes"
	"fmt"
	"path/filepath"
	"sort"
)

// importedPackage is a path of the imported package.
// Represents a row of the import block.
type importedPackage struct {
	alias string // May be empty if package with a same base of the path used in first time.
	path  string // Path of the import.
	name  string // Base of the path, may be with counter suffix.
}

// String returns the row of the import block.
func (ip *importedPackage) String() string {
	if ip.alias != "" {
		return fmt.Sprintf("%s %q", ip.alias, ip.path)
	}

	return fmt.Sprintf("%q", ip.path)
}

// Importer contains information about object's packages, used imports and aliases of it.
type Importer struct {
	currentImportPath string                      // Import path of the current package. May be empty.
	importedPackages  map[string]*importedPackage // Used packages in the current file [used import path] -> *ImportedPackage.
	usedNames         map[string]int              // Used package names in the current file [package name] -> used counter.
	pathToPackage     map[string]*importedPackage // Map of import path to used imported package [import path] -> *ImportedPackage.
}

// New reutrns new epmty Importer without imports and current import path.
// You can set a new current import keeping the import aliases.
func New() *Importer {
	return &Importer{
		currentImportPath: "",
		importedPackages:  make(map[string]*importedPackage, 2),
		usedNames:         make(map[string]int, 2),
		pathToPackage:     make(map[string]*importedPackage, 2),
	}
}

// SetCurrentImportPath sets import path of the current file and
// resets information about imported packages from another file.
func (i *Importer) SetCurrentImportPath(currentImportPath string) {
	i.currentImportPath = currentImportPath
	i.importedPackages = make(map[string]*importedPackage, 2)
}

// RegisterImport registers the import path with its alias.
// Use this when your import suffix not equal to package name.
// Also you can register import without alias.
func (i *Importer) RegisterImport(alias, importPath string) {
	ip := &importedPackage{
		alias: alias,
		path:  importPath,
		name:  alias,
	}

	if ip.name == "" {
		ip.name = filepath.Base(importPath)
	}

	i.pathToPackage[ip.path] = ip
	i.usedNames[ip.name]++
}

// GetPackageName returns name of the package based on import path.
// This method may return empty package name when:
//   - import path is empty
//   - import path equals to current import path (the same package)
//
// This method adds import path to the imported packages list of the file.
func (i *Importer) GetPackageName(importPath string) string {
	if importPath == "" {
		return ""
	}

	if i.currentImportPath == importPath { // Generated file has the same package import.
		return ""
	}

	// In first we check in the stored paths.
	ip, ok := i.pathToPackage[importPath]
	if ok {
		// Importer knows about this import path.
		// Now we check in used list of the file.
		if _, ok = i.importedPackages[importPath]; !ok {
			// So now we use this package in the current file.
			i.importedPackages[importPath] = ip
		}

		return ip.name
	}

	// Importer sees this import path for the first time.
	// We need to name this import and check its aliases.

	// It works if import suffix equals to package name.
	// or you have to use RegisterImport method.
	name := filepath.Base(importPath)

	ip = &importedPackage{
		path: importPath,
		name: name,
	}

	counter := i.usedNames[name]
	counter++

	// Importer don't adds aliases to packages without duplicate package.
	if counter > 1 {
		// Importer already use this name of the import, we have to create alias.
		ip.name = fmt.Sprintf("%s%d", ip.name, counter)
		ip.alias = ip.name
	}

	i.importedPackages[ip.path] = ip
	i.pathToPackage[ip.path] = ip
	i.usedNames[name] = counter

	return ip.name
}

// GetQualifiedObject returns object name with package name.
// Feel free to pass empty import path of the default go types.
func (i *Importer) GetQualifiedObject(importPath, objectName string) string {
	pkg := i.GetPackageName(importPath)
	if pkg == "" {
		return objectName
	}

	return fmt.Sprintf("%s.%s", pkg, objectName)
}

func (i *Importer) printFileImports(buf *bytes.Buffer) {
	list := make([]*importedPackage, len(i.importedPackages))

	index := 0
	for _, ip := range i.importedPackages {
		list[index] = ip
		index++
	}

	sort.Slice(list, func(i, j int) bool { return list[i].path < list[j].path })

	for _, ip := range list {
		buf.WriteByte('\t')
		buf.WriteString(ip.String())
		buf.WriteByte('\n')
	}
}

// GenerateFileImports returns file's imports list.
// Only rows of the import block.
func (i *Importer) GenerateFileImports() string {
	buf := bytes.Buffer{}

	i.printFileImports(&buf)

	return buf.String()
}

// GenerateFileImport returns import block of the file.
func (i *Importer) GenerateFileImport() string {
	buf := bytes.Buffer{}

	buf.WriteString("import (\n")
	i.printFileImports(&buf)
	buf.WriteString(")\n")

	return buf.String()
}
