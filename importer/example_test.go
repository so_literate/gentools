package importer_test

import (
	"fmt"

	"gitlab.com/so_literate/gentools/importer"
)

func Example() {
	imp := importer.New()

	imp.SetCurrentImportPath("host/name/repo/pkg")

	fmt.Println(imp.GetQualifiedObject("", "int64"))                                      // int64
	fmt.Println(imp.GetQualifiedObject("fmt", "Println"))                                 // fmt.Println
	fmt.Println(imp.GetQualifiedObject("host/name/repo/pkg", "LocalType"))                // LocalType
	fmt.Println(imp.GetQualifiedObject("host/name/repo/model", "ExternalType"))           // model.ExternalType
	fmt.Println(imp.GetQualifiedObject("host/name/repo/model/model", "DuplicatePackage")) // model2.DuplicatePackage
	fmt.Println()
	fmt.Println(imp.GenerateFileImport())

	imp.SetCurrentImportPath("host/name/repo/model")

	fmt.Println(imp.GetQualifiedObject("host/name/repo/model", "ExternalType")) // ExternalType
	fmt.Println()
	fmt.Println(imp.GenerateFileImport())

	// Output:
	// int64
	// fmt.Println
	// LocalType
	// model.ExternalType
	// model2.DuplicatePackage
	//
	// import (
	// 	"fmt"
	// 	"host/name/repo/model"
	// 	model2 "host/name/repo/model/model"
	// )
	//
	// ExternalType
	//
	// import (
	// )
}
