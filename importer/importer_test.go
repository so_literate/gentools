package importer_test

import (
	"testing"

	"gitlab.com/so_literate/gentools/importer"
)

const (
	rootPkg     = "host/name/repo"
	modelPkg    = "host/name/repo/model"
	subModelPkg = "host/name/repo/model/model"
)

func TestImporter(t *testing.T) {
	t.Parallel()

	imp := importer.New()

	testCase := func(importPath, objectName, want string) {
		t.Helper()

		got := imp.GetQualifiedObject(importPath, objectName)
		if want != got {
			t.Fatalf("importPath %q:\n\twant: %q\n\tgot:  %q", importPath, want, got)
		}
	}

	testCase("", "int64", "int64")
	testCase("fmt", "Printf", "fmt.Printf")
	testCase(rootPkg, "Obj", "repo.Obj")
	testCase(modelPkg, "Obj", "model.Obj")
	testCase(subModelPkg, "Obj", "model2.Obj")

	imp.SetCurrentImportPath(rootPkg)
	testCase(rootPkg, "Obj", "Obj")
	testCase(subModelPkg, "Obj", "model2.Obj")

	imp.RegisterImport("gopackage", "special/developer/go-package")
	testCase("special/developer/go-package", "Obj", "gopackage.Obj")

	testCase("host/name/gopackage", "Obj", "gopackage2.Obj")

	imp.RegisterImport("", "preregistered/pkg")
	testCase("preregistered/pkg", "Obj", "pkg.Obj")

	imports := imp.GenerateFileImports()

	expImports := `	gopackage2 "host/name/gopackage"
	model2 "host/name/repo/model/model"
	"preregistered/pkg"
	gopackage "special/developer/go-package"
`

	if imports != expImports {
		t.Fatal(imports)
	}

	imp.SetCurrentImportPath("")
	imports = imp.GenerateFileImports()
	if imports != "" {
		t.Fatal(imports)
	}
}
