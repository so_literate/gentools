package writer_test

import (
	"bytes"
	"errors"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/so_literate/gentools/writer"
)

func TestGetFileWriter(t *testing.T) {
	t.Parallel()

	testCase := func(wantContent, file string) {
		t.Helper()

		w, err := writer.GetFileWriter(file)
		if err != nil {
			t.Fatal("writer.GetFileWriter", err)
		}

		if _, err = w.Write([]byte(wantContent)); err != nil {
			t.Fatal("w.Write", err)
		}

		if err = w.Close(); err != nil {
			t.Fatal("w.Close", err)
		}

		gotContent, err := os.ReadFile(file)
		if err != nil {
			t.Fatal("os.ReadFile", err)
		}

		if wantContent != string(gotContent) {
			t.Fatalf("content:\nwant: %q\ngot:  %q", wantContent, string(gotContent))
		}
	}

	dir := t.TempDir()
	testFile := filepath.Join(dir, "some_dir", "file.txt")

	testCase("First line\nSecond line\n", testFile)
	testCase("First line\n", testFile)
}

func TestGetFileWriterErrors(t *testing.T) {
	t.Parallel()

	dir := t.TempDir()
	protectedDir := filepath.Join(dir, "protected")

	err := os.Mkdir(protectedDir, 0o000)
	if err != nil {
		t.Fatal(protectedDir, err)
	}

	protectedDirFile := filepath.Join(protectedDir, "file.txt")

	_, err = writer.GetFileWriter(protectedDirFile)
	if !errors.Is(err, os.ErrPermission) {
		t.Fatal(protectedDirFile, err)
	}

	protectedDirFile = filepath.Join(protectedDir, "inner_dir", "file.txt")
	_, err = writer.GetFileWriter(protectedDirFile)
	if !errors.Is(err, os.ErrPermission) {
		t.Fatal(protectedDirFile, err)
	}

	readOnlyDir := filepath.Join(dir, "read_only")
	err = os.Mkdir(readOnlyDir, 0o500)
	if err != nil { // r-x------
		t.Fatal(readOnlyDir, err)
	}

	readOnlyFile := filepath.Join(readOnlyDir, "inner_dir", "file.txt")
	_, err = writer.GetFileWriter(readOnlyFile)
	if !errors.Is(err, os.ErrPermission) {
		t.Fatal(readOnlyFile, err)
	}
}

func TestGetWriter(t *testing.T) {
	t.Parallel()

	buf := bytes.Buffer{}

	content := "test\n"

	w := writer.GetWriter(&buf)
	if _, err := w.Write([]byte(content)); err != nil {
		t.Fatal(err)
	}

	if buf.String() != content {
		t.Fatal(buf.String())
	}

	if err := w.Close(); err != nil {
		t.Fatal(err)
	}
}
