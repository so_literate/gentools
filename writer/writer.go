// Package writer provides to write in any destinations (io.Writer, file) using the same interface.
package writer

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// It is sort of default config.
var (
	PathToDirPermission  os.FileMode = 0o755 // rwxr-xr-x
	PathToFilePermission os.FileMode = 0o644 // rw-r--r--
)

// NopCloser it's writer without Close method.
type NopCloser struct {
	io.Writer
}

// Close implements io.WriteCloser interface.
func (NopCloser) Close() error { return nil }

var _ io.WriteCloser = NopCloser{}

// GetWriter wraps io.Writer into io.WriteCloser.
func GetWriter(w io.Writer) io.WriteCloser {
	return NopCloser{w}
}

// GetFileWriter returns opened or created file.
// This method can to rewrie new content over previous one
// even the new content is shorter then previous.
func GetFileWriter(pathToFile string) (io.WriteCloser, error) {
	pathToDir := filepath.Dir(pathToFile)

	_, err := os.Stat(pathToDir)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, fmt.Errorf("os.Stat %q: %w", pathToDir, err)
		}

		if err = os.MkdirAll(pathToDir, PathToDirPermission); err != nil { //nolint:gosec // It's ok to create dir by path.
			return nil, fmt.Errorf("os.MkdirAll %q: %w", pathToDir, err)
		}
	}

	//nolint:gosec // It's ok to create or open file with 'Potential file inclusion via variable'.
	file, err := os.OpenFile(pathToFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, PathToFilePermission)
	if err != nil {
		return nil, fmt.Errorf("os.OpenFile: %w", err)
	}

	if err = file.Truncate(0); err != nil {
		return nil, fmt.Errorf("file.Truncate: %w", err)
	}

	if _, err = file.Seek(0, 0); err != nil {
		return nil, fmt.Errorf("file.Seek: %w", err)
	}

	return file, nil
}
