package writer_test

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/so_literate/gentools/writer"
)

func Example() {
	// You have just io.Writer without Close() method, *bytes.Buffer for example.
	var wr io.Writer = os.Stdout

	// Also you have a path to the file.
	pathToFile := "./path/to/file.txt"

	// And you want to write your content into them using the same code.
	var out io.WriteCloser

	if wr != nil {
		out = writer.GetWriter(wr)
	} else {
		var err error

		out, err = writer.GetFileWriter(pathToFile)
		if err != nil {
			fmt.Println("writer.GetFileWriter:", err)
			return
		}
	}

	_, err := out.Write([]byte("Some content\n"))
	if err != nil {
		fmt.Println("out.Write:", err)
		return
	}

	if err = out.Close(); err != nil {
		fmt.Println("out.Close:", err)
		return
	}

	// Output:
	// Some content
}
