// Package logger contains method to create logger for generator base on config.
package logger

import (
	"io"
	"os"

	"github.com/rs/zerolog"
)

// Config logger configuration.
type Config struct {
	Debug      bool      // Prints debug messages of the generator.
	Quiet      bool      // Does not print any messages.
	Pretty     bool      // Prints pretty log messages.
	NoColor    bool      // Prints log messages without color.
	TimeFormat string    // Time format of the pretty writer ("15:04:05" by default).
	Writer     io.Writer // Destination writer of the logger (os.Stderr by default).
}

func setDefaultConfigValues(conf *Config) *Config {
	if conf == nil {
		conf = &Config{
			Debug:   false,
			Quiet:   false,
			Pretty:  true,
			NoColor: false,
		}
	}

	if conf.TimeFormat == "" {
		conf.TimeFormat = "15:04:05" // Just time HH:MM:SS.
	}

	if conf.Writer == nil {
		conf.Writer = os.Stderr
	}

	return conf
}

// New returns new logger based on config.
// Config is an optional feel free to call method without arguments.
func New(config ...*Config) *zerolog.Logger {
	var conf *Config
	if len(config) != 0 {
		conf = config[0]
	}

	conf = setDefaultConfigValues(conf)

	var logWriter io.Writer

	if conf.Pretty {
		logWriter = zerolog.ConsoleWriter{
			Out:        conf.Writer,
			NoColor:    conf.NoColor,
			TimeFormat: conf.TimeFormat,
		}
	} else {
		logWriter = conf.Writer
		zerolog.TimeFieldFormat = conf.TimeFormat
	}

	logger := zerolog.New(logWriter).Level(zerolog.InfoLevel).With().Timestamp().Logger()

	if conf.Debug {
		logger = logger.Level(zerolog.DebugLevel)
	}

	if conf.Quiet {
		logger = logger.Level(zerolog.Disabled)
	}

	return &logger
}
