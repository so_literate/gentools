package logger_test

import (
	"gitlab.com/so_literate/gentools/logger"
)

func Example() {
	conf := &logger.Config{
		Debug:   false,
		Quiet:   false,
		Pretty:  true,
		NoColor: true,
	}

	log := logger.New(conf)

	log.Info().Msg("info")
	log.Debug().Msg("debug")

	// Logger keeps stdout empty so you can print generated code in stdout.

	// Output:
}
