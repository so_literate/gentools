package logger_test

import (
	"bufio"
	"bytes"
	"os"
	"reflect"
	"testing"

	"gitlab.com/so_literate/gentools/logger"
)

func TestConfig(t *testing.T) {
	t.Parallel()

	var got logger.Config

	logger.New(&got)

	want := logger.Config{
		Debug:      false,
		Quiet:      false,
		Pretty:     false,
		NoColor:    false,
		TimeFormat: "15:04:05",
		Writer:     os.Stderr,
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("unexpected config:\n\twant: %#v\n\tgot:  %#v", want, got)
	}

	logger.New()
	// no panic there
}

func TestNew(t *testing.T) {
	t.Parallel()

	buf := bytes.NewBuffer(nil)

	testCase := func(want string) {
		t.Helper()

		lines := make([]string, 0)

		sc := bufio.NewScanner(buf)
		for sc.Scan() {
			lines = append(lines, sc.Text())
		}

		if len(lines) != 1 {
			t.Fatalf("too many lines: %d\n\t%v", len(lines), lines)
		}

		got := lines[0][9:] // cut time part of the log.

		if want != got {
			t.Fatalf("unexpected line:\n\twant: %#v\n\tgot:  %#v", want, got)
		}

		buf.Reset()
	}

	conf := &logger.Config{
		Debug:   false,
		Quiet:   false,
		Pretty:  true,
		NoColor: true,
		Writer:  buf,
	}

	// Without debug level.
	log := logger.New(conf)

	log.Debug().Msg("debug")
	log.Info().Msg("info")
	testCase("INF info")

	// With debug level
	conf.Debug = true
	log = logger.New(conf)

	log.Debug().Msg("debug")
	testCase("DBG debug")

	// Quiet logger
	conf.Quiet = true

	log = logger.New(conf)
	log.Error().Msg("error")

	if buf.String() != "" {
		t.Fatalf("unexpected buffer content: %s", buf.String())
	}

	// No pretty Logger
	conf.Pretty = false
	conf.Quiet = false
	log = logger.New(conf)

	log.Warn().Msg("warning")

	t.Log(buf.String())
}
