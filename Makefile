.PHONY: lints lints_fix test coverage

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml
	cd ./logger && golangci-lint run -c ../.linters.yml
	cd ./packager && golangci-lint run -c ../.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml
	cd ./logger && golangci-lint run --fix -c ../.linters.yml
	cd ./packager && golangci-lint run --fix -c ../.linters.yml

test:
	go test -cover ./...
	cd ./logger && go test -cover ./...
	cd ./packager && go test -cover ./...

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'
