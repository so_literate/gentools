package pkg

import (
	"gitlab.com/so_literate/gentools/packager/testdata/pkg/pkginner"
	"gitlab.com/so_literate/gentools/packager/testdata/pkgexternal"
)

// ImportedType type imported from outside package.
type ImportedType struct {
	// Top comment.
	Field string // Side comment.

	// OnlyTop comment.
	OnlyTop string

	OnlySide string // Side comment.

	FromFile TypeFromAnotherFile // Comment.
}

// Method comment of method.
func (it *ImportedType) Method(i int) string {
	return it.Field
}

// Types not used in "models" packkage directly.

type UseInner struct {
	Inner pkginner.InnerType
}

type UseExternal struct {
	External pkgexternal.ExternalType
}
