package enum

type EnumInt int

const (
	// EnumIntFirst doc.
	EnumIntFirst  EnumInt = iota + 1
	EnumIntSecond         // Comment.
	// EnumAlphaName doc.
	EnumAlphaName // Comment.
)

type EnumString string

// EnumStringFirst docs, can't see this.
const EnumStringFirst EnumString = "FIRST"

const EnumStringSecond EnumString = "SECOND" // Comment SECOND.

// EnumOrderThird doc, can't see this.
const EnumOrderThird EnumString = "THIRD" // Comment THIRD.
