package models

import (
	"gitlab.com/so_literate/gentools/packager/testdata/pkg"
)

// EmbeddedType embedded into SomeType.
type EmbeddedType struct {
	EmbeddedField string
}

// SomeType doc of the type.
type SomeType struct {
	EmbeddedType // Side comment.

	// Top comment.
	imported pkg.ImportedType `json:"imported,omitempty"` // Side comment.
	// Top comment.
	Field string `json:"field,omitempty"`
}

// Iface comments.
type Iface interface {
	// Method comment.
	Method(*SomeType)
}
