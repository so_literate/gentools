// Package packager contains some helpers to parse source code using golang.org/x/tools/go/packages.
// The package provides method to find Ast Generic Declaration of the object from "go/types"
// and method to find "go/types" declaration using Ast Expression.
package packager

import (
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"go/types"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"sync"

	"golang.org/x/tools/go/packages"
)

// DefaultParserParseFile default go/packages func to parse files.
func DefaultParserParseFile(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
	const mode = parser.AllErrors | parser.ParseComments
	return parser.ParseFile(fset, filename, src, mode) //nolint:wrapcheck // no need to add context of the error.
}

// ErrWrongPackagesAmount returns when packages parser returned unexpected amount of parsed packages.
var ErrWrongPackagesAmount = errors.New("wrong amount of parsed packages")

type pkgParser struct {
	mu    sync.Mutex
	files []*ast.File
	pkg   *packages.Package
}

func (p *pkgParser) parseFileFn(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
	astFile, err := DefaultParserParseFile(fset, filename, src)
	if err != nil {
		return nil, err // no need additional context.
	}

	p.mu.Lock()
	p.files = append(p.files, astFile)
	p.mu.Unlock()

	return astFile, nil
}

func (p *pkgParser) parse(parentConfig *packages.Config, path string) error {
	config := *parentConfig

	// Need types info for searching types and scopes.
	// NeedSyntax for FindConstValuesOfType.
	config.Mode = packages.NeedName | packages.NeedImports | packages.NeedTypes | packages.NeedTypesInfo | packages.NeedSyntax
	config.Dir = path
	config.ParseFile = p.parseFileFn

	pkgs, err := packages.Load(&config)
	if err != nil {
		return fmt.Errorf("packages.Load: %w", err)
	}

	if len(pkgs) != 1 {
		return fmt.Errorf("%w: want 1, got %d", ErrWrongPackagesAmount, len(pkgs))
	}

	p.pkg = pkgs[0]

	return nil
}

// Packager contains all parsed ast files and all dependency package
// with access to fine any type or ast declaration.
type Packager struct {
	mu         sync.Mutex
	pkgToParse map[string]struct{}

	// Patterns to skip for parsing, GOCACHE dir by default.
	SkipPatterns []*regexp.Regexp
	// Parsed ast files grouped by package path.
	AstByPackage map[string][]*ast.File
	// List of the parsed packages with access by path.
	Pkgs map[string]*packages.Package

	// Origin function from packages.Config.
	OriginParseFile func(fset *token.FileSet, filename string, src []byte) (*ast.File, error)
}

// New creates new packager to parse go packages with access to every parsed ast file.
// It takes list of packages (regexp) to ignore in recursive scanning.
func New(igonrePackages ...string) (*Packager, error) {
	res := &Packager{
		pkgToParse:   make(map[string]struct{}, 4),
		AstByPackage: make(map[string][]*ast.File, 4),
	}

	gocache := getCacheDirRegexp()
	if gocache != "" {
		igonrePackages = append(igonrePackages, gocache)
	}

	res.SkipPatterns = make([]*regexp.Regexp, len(igonrePackages))

	for i, expr := range igonrePackages {
		compiledExpr, err := regexp.Compile(expr)
		if err != nil {
			return nil, fmt.Errorf("regexp.Compile %q: %w", expr, err)
		}

		res.SkipPatterns[i] = compiledExpr
	}

	return res, nil
}

func getGoCacheFromGoEnv() string {
	out, err := exec.Command("go", "env", "GOCACHE").CombinedOutput()
	if err != nil {
		return ""
	}

	return strings.TrimSpace(string(out))
}

// getCacheDirRegexp returns regexp string for go-cache directory.
func getCacheDirRegexp() string {
	expr := getGoCacheFromGoEnv()

	if expr == "" {
		expr = os.Getenv("GOCACHE")
	}

	if expr == "" {
		return ""
	}

	expr = strings.ReplaceAll(expr, ".", `\.`)
	expr += ".*"

	_, err := regexp.Compile(expr)
	if err != nil {
		return ""
	}

	return expr
}

// parseFile implements default go/packages func but keeps every parsed path of the package.
func (p *Packager) parseFile(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
	var astFile *ast.File
	var err error

	if p.OriginParseFile != nil {
		astFile, err = p.OriginParseFile(fset, filename, src)
	} else {
		astFile, err = DefaultParserParseFile(fset, filename, src)
	}

	if err != nil {
		return nil, fmt.Errorf("failed to ParseFile: %w", err)
	}

	pathToPackage := filepath.Dir(filename)

	for _, skip := range p.SkipPatterns {
		if skip.MatchString(pathToPackage) {
			return astFile, nil
		}
	}

	p.mu.Lock()
	p.pkgToParse[pathToPackage] = struct{}{}
	p.mu.Unlock()

	return astFile, nil
}

// PackagesLoad looks like a packages.Load method with additional parsing of the each dependency package.
// Returns parsed packages using parser config. Field Pkgs contains all parsed packages
// and field AstByPackage contains all parsed ast.Files.
func (p *Packager) PackagesLoad(config *packages.Config, patterns ...string) ([]*packages.Package, error) {
	if p.pkgToParse == nil {
		p.pkgToParse = make(map[string]struct{}, 4)
	}

	realConfig := *config

	// Need to parse all deps of the parent package.
	if realConfig.Mode&packages.NeedDeps == 0 {
		realConfig.Mode |= packages.NeedDeps
	}

	// Replace parse file function.
	p.OriginParseFile = realConfig.ParseFile
	realConfig.ParseFile = p.parseFile

	pkgs, err := packages.Load(&realConfig, patterns...)
	if err != nil {
		return nil, fmt.Errorf("packages.Load: %w", err)
	}

	p.Pkgs = make(map[string]*packages.Package, len(p.pkgToParse))
	p.AstByPackage = make(map[string][]*ast.File, len(p.pkgToParse))

	// Now we need to parse each dependency package.
	for pkg := range p.pkgToParse {
		pr := new(pkgParser)
		if err = pr.parse(config, pkg); err != nil {
			return nil, fmt.Errorf("parse pkg %q: %w", pkg, err)
		}

		p.Pkgs[pr.pkg.PkgPath] = pr.pkg
		p.AstByPackage[pr.pkg.PkgPath] = pr.files
	}

	// Returns result that we got using user config.
	return pkgs, nil
}

func (p *Packager) getGenDeclOfType(file *ast.File, typeName string) *ast.GenDecl {
	for _, decl := range file.Decls {
		genDecl, isGenDecl := decl.(*ast.GenDecl)
		if !isGenDecl {
			continue
		}

		for _, spec := range genDecl.Specs {
			typeSpec, isTypeSpec := spec.(*ast.TypeSpec)
			if !isTypeSpec {
				continue
			}

			if typeSpec.Name.Name == typeName {
				return genDecl
			}
		}
	}

	return nil
}

// GetGenDeclOfType looks for a generic declaration of the type.
// Takes full module path of the package and name of the type.
// For example you can find type declaration in the package:
//
//	GetGenDecl("path/to/package", "TypeName")
//
// Returns nil if not found.
// ast.GenDecl contains Doc and Comments block of the type.
func (p *Packager) GetGenDeclOfType(packagePath, scopeName string) *ast.GenDecl {
	p.mu.Lock()
	files := p.AstByPackage[packagePath]
	p.mu.Unlock()

	for _, file := range files {
		obj := file.Scope.Lookup(scopeName)
		if obj != nil {
			return p.getGenDeclOfType(file, scopeName)
		}
	}

	return nil
}

// GetTypeOfExpr looks for types.Type representation of the ast.Expr.
// For example you can get types description of the ast object.
//
//	methodType = GetTypeOfExpr((*ast.InterfaceType).Methods.List[0].Type) // methodType.(types.Type)
//
// Returns nil if not found.
// types.Type contains more useful abstractions of the type.
func (p *Packager) GetTypeOfExpr(expr ast.Expr) types.Type {
	for _, pkg := range p.Pkgs {
		if res := pkg.TypesInfo.TypeOf(expr); res != nil {
			return res
		}
	}

	return nil
}

// fillConstOrder fills order of the const names as in the ast.File.
func fillConstOrder(syntaxFiles []*ast.File, names map[string]*ConstValue) {
	order := 0

	for _, file := range syntaxFiles {
		for _, decl := range file.Decls {
			ast.Inspect(decl, func(n ast.Node) bool {
				valueSpec, isValueSpec := n.(*ast.ValueSpec)
				if !isValueSpec {
					return true // keep search.
				}

				if len(valueSpec.Names) != 1 {
					return false
				}

				name := valueSpec.Names[0].Name

				value, isConstName := names[name]
				if !isConstName {
					return false
				}

				value.order = order
				value.Spec = valueSpec

				order++

				return false
			})
		}
	}
}

// ConstValue constant value of the type.
type ConstValue struct {
	Type *types.Const   // go/types representation of the value, contains type description.
	Spec *ast.ValueSpec // go/ast representation of the value, contains comments.

	order int
}

// FindConstValuesOfType returns constant values of the named type.
// Useful if you want to find "Enum" values of the type.
func (p *Packager) FindConstValuesOfType(namedType *types.Named) []*ConstValue {
	pkg, ok := p.Pkgs[namedType.Obj().Pkg().Path()]
	if !ok {
		return nil
	}

	values := make(map[string]*ConstValue, 0)

	for _, name := range pkg.Types.Scope().Names() {
		obj := pkg.Types.Scope().Lookup(name)

		constType, isConst := obj.(*types.Const)
		if !isConst {
			continue
		}

		constTypeNamedType, isNamedType := constType.Type().(*types.Named)
		if !isNamedType {
			continue
		}

		if constTypeNamedType.Obj().Name() != namedType.Obj().Name() {
			continue
		}

		values[constType.Name()] = &ConstValue{
			Type:  constType,
			Spec:  nil,
			order: 0,
		}
	}

	if len(values) == 0 {
		return nil
	}

	// Now we need to fix order of the const slice
	// because pkg.Types.Scope().Names() returns sorted list of names.
	fillConstOrder(pkg.Syntax, values)

	res := make([]*ConstValue, len(values))

	i := 0
	for _, value := range values {
		res[i] = value
		i++
	}

	sort.Slice(res, func(i, j int) bool {
		return res[i].order < res[j].order
	})

	return res
}
