module gitlab.com/so_literate/gentools/packager

go 1.22

require golang.org/x/tools v0.19.0

require golang.org/x/mod v0.16.0 // indirect
