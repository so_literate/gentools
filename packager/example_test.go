package packager_test

import (
	"fmt"
	"go/ast"
	"go/types"

	"gitlab.com/so_literate/gentools/packager"

	"golang.org/x/tools/go/packages"
)

func Example() {
	// Create your config of the x/tools/go/packages parser.
	pkgsConfig := packages.Config{
		// I want to get types and pkg info of the dir in my project.
		Mode: packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
		Dir:  "./testdata/models",
	}

	pkgr, err := packager.New()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	pkgs, err := pkgr.PackagesLoad(&pkgsConfig)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Number of packages:", len(pkgs))

	// Just work with your parsed package using "go/types" abstactions:
	someType := pkgs[0].Types.Scope().Lookup("SomeType")
	fmt.Println("someType:", someType.Pkg().Path(), someType.Name())

	// If you need you can get *ast.GenDecl of any type from any package.
	// *ast.GenDecl contains docs and every comments of the type.
	astGen := pkgr.GetGenDeclOfType(someType.Pkg().Path(), someType.Name())
	fmt.Print("Top comments of the SomeType: ", astGen.Doc.Text())

	someTypeBackConverted := pkgr.GetTypeOfExpr(astGen.Specs[0].(*ast.TypeSpec).Type)

	// someTypeBackConverted is the same Underlying type of the Named "SomeType" struct.
	fmt.Println(someTypeBackConverted.String() == someType.Type().Underlying().String())

	// Output:
	// Number of packages: 1
	// someType: gitlab.com/so_literate/gentools/packager/testdata/models SomeType
	// Top comments of the SomeType: SomeType doc of the type.
	// true
}

func ExamplePackager_FindConstValuesOfType() {
	pkgsConfig := packages.Config{
		Mode: packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
		Dir:  "./testdata/enum",
	}

	pkgr, err := packager.New()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	pkgs, err := pkgr.PackagesLoad(&pkgsConfig)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// just get your "enum" type.
	// Enum is a basic type with const values in the package.
	enum := pkgs[0].Types.Scope().Lookup("EnumInt").Type().(*types.Named)

	consts := pkgr.FindConstValuesOfType(enum)

	for _, value := range consts {
		fmt.Printf("%s: %s - %q\n", value.Type.Name(), value.Type.Val().String(), value.Spec.Comment.Text())
	}

	// Output:
	// EnumIntFirst: 1 - ""
	// EnumIntSecond: 2 - "Comment.\n"
	// EnumAlphaName: 3 - "Comment.\n"
}
