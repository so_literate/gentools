package packager_test

import (
	"go/ast"
	"go/token"
	"go/types"
	"strings"
	"testing"

	"golang.org/x/tools/go/packages"

	"gitlab.com/so_literate/gentools/packager"
)

const (
	packagePathTestdataModels      = "gitlab.com/so_literate/gentools/packager/testdata/models"
	packagePathTestdataPkg         = "gitlab.com/so_literate/gentools/packager/testdata/pkg"
	packagePathTestdataPkgInner    = "gitlab.com/so_literate/gentools/packager/testdata/pkg/pkginner"
	packagePathTestdataPkgExternal = "gitlab.com/so_literate/gentools/packager/testdata/pkgexternal"
)

var testPackagesConfig = &packages.Config{
	// I want to get types and pkg info of the dir in my project.
	Mode: packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
	Dir:  "./testdata/models",
}

func checkParserResult(t *testing.T, pkgs []*packages.Package) {
	t.Helper()

	if len(pkgs) != 1 {
		t.Fatalf("wrong number of packages: %d", len(pkgs))
	}

	pkg := pkgs[0]

	if pkg.PkgPath != packagePathTestdataModels {
		t.Fatalf("worong package path:\nwant %q\ngot:  %q", packagePathTestdataModels, pkg.PkgPath)
	}
}

//nolint:gocognit,gocyclo,maintidx // sort of example test, little ugly (>30 points).
func TestPackagesLoad_TypicalCase(t *testing.T) {
	t.Parallel()

	p, err := packager.New()
	if err != nil {
		t.Fatal(err.Error())
	}

	pkgs, err := p.PackagesLoad(testPackagesConfig)
	if err != nil {
		t.Fatal(err)
	}

	checkParserResult(t, pkgs)

	if len(p.AstByPackage[packagePathTestdataModels]) != 1 {
		t.Fatalf("%#v", p.AstByPackage)
	}

	if len(p.AstByPackage[packagePathTestdataPkg]) != 2 {
		t.Fatalf("%#v", p.AstByPackage)
	}

	if len(p.AstByPackage[packagePathTestdataPkgInner]) != 1 {
		t.Fatalf("%#v", p.AstByPackage)
	}

	if len(p.AstByPackage[packagePathTestdataPkgExternal]) != 2 {
		t.Fatalf("%#v", p.AstByPackage)
	}

	if len(p.Pkgs) != 4 {
		t.Fatalf("%#v", p.Pkgs)
	}

	// Got interface as "types".
	obj := pkgs[0].Types.Scope().Lookup("Iface")

	if obj.Pkg().Path() != packagePathTestdataModels {
		t.Fatal(obj.Pkg().Path())
	}
	if obj.Name() != "Iface" {
		t.Fatal(obj.Name())
	}

	// Converted interface to "ast" from "types".
	genDecl := p.GetGenDeclOfType(obj.Pkg().Path(), obj.Name())
	if genDecl == nil {
		t.Fatal("genDecl is nil")
	}

	if genDecl.Doc.Text() != "Iface comments.\n" { // You can get comment of the type.
		t.Fatal(genDecl.Doc.Text())
	}

	method := genDecl.Specs[0].(*ast.TypeSpec).Type.(*ast.InterfaceType).Methods.List[0]

	if method.Doc.Text() != "Method comment.\n" { // You can get comment of the method.
		t.Fatal(method.Doc.Text())
	}

	// Convert method to "types" from "ast".
	methodType := p.GetTypeOfExpr(method.Type)
	if methodType == nil {
		t.Fatal("methodType is nil")
	}

	namedParam := methodType.(*types.Signature).Params().At(0).Type().(*types.Pointer).Elem().(*types.Named)

	if namedParam.Obj().Pkg().Path() != packagePathTestdataModels {
		t.Fatal(namedParam.Obj().Pkg().Path())
	}
	if namedParam.Obj().Name() != "SomeType" {
		t.Fatal(namedParam.Obj().Name())
	}

	// Convert param of the method to "ast" from "types".
	paramDecl := p.GetGenDeclOfType(namedParam.Obj().Pkg().Path(), namedParam.Obj().Name())
	if paramDecl == nil {
		t.Fatal("paramDecl is nil")
	}

	if paramDecl.Doc.Text() != "SomeType doc of the type.\n" { // You can get comment of the type (again).
		t.Fatal(paramDecl.Doc.Text())
	}

	// Compare "types" fields with "ast" fields.
	assert := func(fieldTypes *types.Var, fieldAst *ast.Field, wantEmbedded bool, wantName, wantTag, wantDoc, wantComment string) {
		t.Helper()

		if fieldTypes.Embedded() != wantEmbedded {
			t.Fatalf("wrong embedded: %v != %v", fieldTypes.Embedded(), wantEmbedded)
		}

		if fieldTypes.Name() != wantName {
			t.Fatalf("wrong name: %q != %q", fieldTypes.Name(), wantName)
		}

		if wantTag != "" {
			if fieldAst.Tag.Value != wantTag {
				t.Fatalf("wrong tag: %q != %q", fieldAst.Tag.Value, wantTag)
			}
		} else {
			if fieldAst.Tag != nil {
				t.Fatalf("not empty tag: %#v", fieldAst.Tag)
			}
		}

		if fieldAst.Doc.Text() != wantDoc {
			t.Fatalf("wrong doc: %q != %q", fieldAst.Doc.Text(), wantDoc)
		}

		if fieldAst.Comment.Text() != wantComment {
			t.Fatalf("wrong comment: %q != %q", fieldAst.Comment.Text(), wantComment)
		}
	}

	structAst := paramDecl.Specs[0].(*ast.TypeSpec).Type.(*ast.StructType)
	structTypes := namedParam.Underlying().(*types.Struct)

	assert(
		structTypes.Field(0),
		structAst.Fields.List[0],
		true,
		"EmbeddedType",
		"",
		"",
		"Side comment.\n",
	)

	assert(
		structTypes.Field(1),
		structAst.Fields.List[1],
		false,
		"imported",
		"`json:\"imported,omitempty\"`",
		"Top comment.\n",
		"Side comment.\n",
	)

	assert(
		structTypes.Field(2),
		structAst.Fields.List[2],
		false,
		"Field",
		"`json:\"field,omitempty\"`",
		"Top comment.\n",
		"",
	)

	importedFieldType := structTypes.Field(1).Type().(*types.Named)

	if importedFieldType.Obj().Pkg().Path() != packagePathTestdataPkg {
		t.Fatal(importedFieldType.Obj().Pkg().Path())
	}
	if importedFieldType.Obj().Name() != "ImportedType" {
		t.Fatal(importedFieldType.Obj().Name())
	}

	// Convert "types" field of the struct to "ast".
	importedGenDecl := p.GetGenDeclOfType(importedFieldType.Obj().Pkg().Path(), importedFieldType.Obj().Name())
	if importedGenDecl == nil {
		t.Fatal("importedGenDecl is nil")
	}

	// You can get comment of the type from outside package.
	if importedGenDecl.Doc.Text() != "ImportedType type imported from outside package.\n" {
		t.Fatal(importedGenDecl.Doc.Text())
	}

	importedType := p.GetTypeOfExpr(importedGenDecl.Specs[0].(*ast.TypeSpec).Type)
	if importedType == nil {
		t.Fatal("importedType is nil")
	}

	if importedType.String() != importedFieldType.Underlying().String() {
		t.Fatalf("wrong back converted type:\n\twant: %s\n\tgot:  %s", importedType, importedFieldType.Underlying())
	}

	// Just try to get random type.
	randomTypeGenDecl := p.GetGenDeclOfType(packagePathTestdataPkgExternal, "NotUsedAnyWhere")
	if randomTypeGenDecl == nil {
		t.Fatal("randomTypeGenDecl is nil")
	}

	randomType := p.GetTypeOfExpr(randomTypeGenDecl.Specs[0].(*ast.TypeSpec).Type)
	if randomType == nil {
		t.Fatal("randomType is nil")
	}
}

func TestPackagesLoad_OriginParseFile(t *testing.T) {
	t.Parallel()

	var isParseFileCalled bool

	modConfig := *testPackagesConfig

	modConfig.ParseFile = func(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
		isParseFileCalled = true
		return packager.DefaultParserParseFile(fset, filename, src)
	}

	p, err := packager.New()
	if err != nil {
		t.Fatal(err.Error())
	}

	pkgs, err := p.PackagesLoad(&modConfig)
	if err != nil {
		t.Fatal(err.Error())
	}

	checkParserResult(t, pkgs)

	if !isParseFileCalled {
		t.Fatal("func ParseFile is not called")
	}
}

func TestPackager_NoPanicAndError(t *testing.T) {
	t.Parallel()

	config := &packages.Config{
		Dir: "./not_exist/dir",
	}

	p := &packager.Packager{}

	pkgs, err := p.PackagesLoad(config)
	if err == nil || !strings.Contains(err.Error(), config.Dir) {
		t.Fatalf("wrong error: %v", err)
	}

	if len(pkgs) != 0 {
		t.Fatalf("wrong number of parsed packages: %#v", pkgs)
	}

	genDecl := p.GetGenDeclOfType("path/to/package", "someName")
	if genDecl != nil {
		t.Fatalf("%#v", genDecl)
	}

	object := p.GetTypeOfExpr(new(ast.TypeSpec).Type)
	if object != nil {
		t.Fatalf("%#v", object)
	}
}

func TestFindConstValuesOfType(t *testing.T) {
	t.Parallel()

	config := *testPackagesConfig
	config.Dir = "./testdata/enum"

	p, err := packager.New()
	if err != nil {
		t.Fatal(err.Error())
	}

	pkgs, err := p.PackagesLoad(&config)
	if err != nil {
		t.Fatal(err)
	}

	pkg := pkgs[0]

	type testCaseConst struct {
		name    string
		value   string
		doc     string
		comment string
	}

	type testCase struct {
		typeName string
		consts   []testCaseConst
	}

	runTest := func(params testCase) {
		t.Helper()

		enumObj := pkg.Types.Scope().Lookup(params.typeName)
		if enumObj == nil {
			t.Fatalf("type not found: %q", params.typeName)
		}

		named, ok := enumObj.Type().(*types.Named)
		if !ok {
			t.Fatalf("type is not a Named Type: %#v", enumObj.Type())
		}

		res := p.FindConstValuesOfType(named)

		if len(params.consts) != len(res) {
			t.Fatalf("unexpected number of the values: want %d, got %d", len(params.consts), len(res))
		}

		for i, want := range params.consts {
			got := res[i]

			if want.name != got.Type.Name() {
				t.Fatalf("unexpected name: want %q, got %q", want.name, got.Type.Name())
			}

			if want.value != got.Type.Val().String() {
				t.Fatalf("unaexpected value: want %q, got %q", want.value, got.Type.Val().String())
			}

			if want.doc != got.Spec.Doc.Text() {
				t.Fatalf("unaexpected doc: want %q, got %q\n\t%#v", want.doc, got.Spec.Doc.Text(), got.Spec)
			}

			if want.comment != got.Spec.Comment.Text() {
				t.Fatalf("unaexpected comment: want %q, got %q", want.comment, got.Spec.Comment.Text())
			}
		}
	}

	cases := []testCase{
		{
			typeName: "EnumInt",
			consts: []testCaseConst{
				{
					name:  "EnumIntFirst",
					value: "1",
					doc:   "EnumIntFirst doc.\n",
				},
				{
					name:    "EnumIntSecond",
					value:   "2",
					comment: "Comment.\n",
				},
				{
					name:    "EnumAlphaName",
					value:   "3",
					doc:     "EnumAlphaName doc.\n",
					comment: "Comment.\n",
				},
			},
		},
		{
			typeName: "EnumString",
			consts: []testCaseConst{
				{
					name:  "EnumStringFirst",
					value: `"FIRST"`,
				},
				{
					name:    "EnumStringSecond",
					value:   `"SECOND"`,
					comment: "Comment SECOND.\n",
				},
				{
					name:    "EnumOrderThird",
					value:   `"THIRD"`,
					comment: "Comment THIRD.\n",
				},
			},
		},
	}

	for _, test := range cases {
		runTest(test)
	}
}

//nolint:paralleltest // no paralleltest because t.Setenv.
func TestGetCacheDirRegexp(t *testing.T) {
	testCase := func(envValue string, want string) {
		t.Helper()
		t.Setenv("GOCACHE", envValue)

		p, err := packager.New()
		if err != nil {
			t.Error("packager.New", err)
		}

		if len(p.SkipPatterns) != 1 {
			t.Errorf("wrong length: %d != 1", len(p.SkipPatterns))
		}

		got := p.SkipPatterns[0].String()
		if want != got {
			t.Errorf("%q != %q", want, got)
		}
	}

	testCase("/some/dir/cache/go-build", "/some/dir/cache/go-build.*")
	testCase("/home/user/.cache/go-build", "/home/user/\\.cache/go-build.*")
}
