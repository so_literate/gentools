Packager
========

It is just a wrapper over `golang.org/x/tools/go/packages` but contains all dependent packages with types info.

Packager provides access to random ast generic declaration by its pkg path and type name.
It usefull when you want to work with `go/types` abstractions with access to `*ast.GenDecl` to
get Doc and Comments of the Type/Field/etc.

Also you can convert your `ast.Expr` to `types.Type` back.

# Example

```go
package main

import (
	"fmt"
	"go/ast"

	"gitlab.com/so_literate/gentools/packager"

	"golang.org/x/tools/go/packages"
)

func main() {
	// Create your config of the x/tools/go/packages parser.
	pkgsConfig := packages.Config{
		// I want to get types and pkg info of the dir in my project.
		Mode: packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
		Dir:  "./testdata/models",
	}

	pkgr, err := packager.New()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	pkgs, err := pkgr.PackagesLoad(&pkgsConfig)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Number of packages:", len(pkgs))

	// Just work with your parsed package using "go/types" abstactions:
	someType := pkgs[0].Types.Scope().Lookup("SomeType")
	fmt.Println("someType:", someType.Pkg().Path(), someType.Name())

	// If you need you can get *ast.GenDecl of any type from any package.
	// *ast.GenDecl contains docs and every comments of the type.
	astGen := pkgr.GetGenDeclOfType(someType.Pkg().Path(), someType.Name())
	fmt.Print("Top comments of the SomeType: ", astGen.Doc.Text())

	someTypeBackConverted := pkgr.GetTypeOfExpr(astGen.Specs[0].(*ast.TypeSpec).Type)

	// someTypeBackConverted is the same Underlying type of the Named "SomeType" struct.
	fmt.Println(someTypeBackConverted.String() == someType.Type().Underlying().String())
}
```
