gentools
========

gentools is a set of helper tools for Go code generators.

# Tools

## Importer

`gitlab.com/so_literate/gentools/importer`

Importer controls your import block of the generated code and objects from external packages.

```go
import "gitlab.com/so_literate/gentools/importer"

func main() {
	imp := importer.New()

	imp.SetCurrentImportPath("host/name/repo/pkg")

	imp.GetQualifiedObject("", "int64")                                      // int64
	imp.GetQualifiedObject("fmt", "Println")                                 // fmt.Println
	imp.GetQualifiedObject("host/name/repo/pkg", "LocalType")                // LocalType
	imp.GetQualifiedObject("host/name/repo/model", "ExternalType")           // model.ExternalType
	imp.GetQualifiedObject("host/name/repo/model/model", "DuplicatePackage") // model2.DuplicatePackage

	imp.GenerateFileImport() // import("fmt", "host/name/repo/model", model2 "host/name/repo/model/model")

	imp.SetCurrentImportPath("host/name/repo/model")
	imp.GetQualifiedObject("host/name/repo/model", "ExternalType") // ExternalType
	imp.GenerateFileImport() // import()
}
```

## Writer

`gitlab.com/so_literate/gentools/writer`

Writer provides to write in any destinations (io.Writer, file) using the same interface.

```go
import (
	"fmt"
	"io"
	"os"

	"gitlab.com/so_literate/gentools/writer"
)


func main() {
	// You have just io.Writer without Close() method, *bytes.Buffer for example.
	var wr io.Writer = os.Stdout

	// Also you have a path to the file.
	pathToFile := "./path/to/file.txt"

	// And you want to write your content using the same code.
	var out io.WriteCloser

	if wr != nil {
		out = writer.GetWriter(wr)
	} else {
		var err error

		out, err = writer.GetFileWriter(pathToFile)
		if err != nil {
			fmt.Println("writer.GetFileWriter:", err)
			return
		}
	}

	_, err := out.Write([]byte("Some content\n"))
	if err != nil {
		fmt.Println("out.Write:", err)
		return
	}

	if err = out.Close(); err != nil {
		fmt.Println("out.Close:", err)
		return
	}
}
```

## Logger

`gitlab.com/so_literate/gentools/logger`

Logger is a constructor of the zerolog logger with simple configurator.

```go
import "gitlab.com/so_literate/gentools/logger"

func main() {
	conf := logger.Config{
		Debug:   false,
		Quiet:   false,
		Pretty:  true,
		NoColor: false,
	}

	log := logger.New(&conf)

	log.Info().Msg("info")
	log.Debug().Msg("debug") // no print this Debug: false.

	// Logger keeps stdout empty so you can print generated code in stdout.
}
```

## Packager

`gitlab.com/so_literate/gentools/packager`

It's a wrapper of the `golang.org/x/tools/go/packages` package but it contains each parsed file and package.

Packager provides access to random ast generic declaration by its pkg path and type name.
It usefull when you want to work with `go/types` abstractions with access to `*ast.GenDecl` to
get Doc and Comments of the Type/Field/etc.

Also you can convert your `ast.Expr` to `types.Type` back and find all values of the golang "enum" type.

```go
package main

import (
	"fmt"
	"go/ast"

	"gitlab.com/so_literate/gentools/packager"

	"golang.org/x/tools/go/packages"
)

func main() {
	// Create your config of the x/tools/go/packages parser.
	pkgsConfig := packages.Config{
		// I want to get types and pkg info of the dir in my project.
		Mode: packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
		Dir:  "./testdata/models",
	}

	pkgr, err := packager.New()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	pkgs, err := pkgr.PackagesLoad(&pkgsConfig)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Number of packages:", len(pkgs))

	// Just work with your parsed package using "go/types" abstactions:
	someType := pkgs[0].Types.Scope().Lookup("SomeType")
	fmt.Println("someType:", someType.Pkg().Path(), someType.Name())

	// If you need you can get *ast.GenDecl of any type from any package.
	// *ast.GenDecl contains docs and every comments of the type.
	astGen := pkgr.GetGenDeclOfType(someType.Pkg().Path(), someType.Name())
	fmt.Print("Top comments of the SomeType: ", astGen.Doc.Text())

	someTypeBackConverted := pkgr.GetTypeOfExpr(astGen.Specs[0].(*ast.TypeSpec).Type)

	// someTypeBackConverted is the same Underlying type of the Named "SomeType" struct.
	fmt.Println(someTypeBackConverted.String() == someType.Type().Underlying().String())
}
```
